package street;

import vehicles.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

@SuppressWarnings("FieldCanBeLocal")
public class TrafficLight {

    // four arrays, one for each direction of traffic
    // each can hold 5 vehicles
    private Vehicle[] north = new Vehicle[5];
    private Vehicle[] south = new Vehicle[5];
    private Vehicle[] east = new Vehicle[5];
    private Vehicle[] west = new Vehicle[5];

    // indicates whether the simulation is running or not
    private boolean running = false;

    // indicates whether our directional lights are green
    private boolean northSouthGreen = false;
    private boolean eastWestGreen = false;

    // how long does our light stay open for? (secs)
    private final int LIGHT_DURATION = 25;

    // default constructor
    public TrafficLight() {

        // start our simulation
        this.running = true;

        // generate our vehicles in each direction
        generateVehicles(north);
        generateVehicles(south);
        generateVehicles(east);
        generateVehicles(west);

        northSouthGreen = true;

    }

    // fill each direction of the light with 1-5 vehicles, each of a random type
    private void generateVehicles(Vehicle[] lane) {

        // how many vehicles will be in our array?
        int numOfVehicles = (int) (Math.random() * ((5 - 1) + 1)) + 1;

        for (int i = 0; i < numOfVehicles; i++) {

            // our three types of vehicles: car, semi, or truck (excluding semi)
            int vehicleType = (int) (Math.random() * ((3 - 1) + 1)) + 1;
            randomVehicle(lane, i, vehicleType);
        }
    }

    // method to display the state of our intersection
    public void displayIntersection() {
        if (northSouthGreen) {
            System.out.println("NS: Green");
            System.out.println("EW: Red");
        } else if (eastWestGreen) {
            System.out.println("NS: Red");
            System.out.println("EW: Green");
        } else {
            System.out.println("NS: Red");
            System.out.println("EW: Red");
        }
        displayLane(north, "north");
        System.out.print("----------     ----------\n");
        displayLane(west, "west");
        displayLane(east, "east");
        System.out.print("\n----------     ----------\n");
        displayLane(south, "south");
    }

    // method to build our lanes
    private void displayLane(Vehicle[] array, String direction) {
        if (direction.equals("north")) {
            for (int i = array.length - 1; i >= 0; i--) {
                printVerLane(array, i);
            }
        } else if (direction.equals("south")) {
            for (int i = 0; i < array.length; i++) {
                printVerLane(array, i);
            }
        } else if (direction.equals("east")) {
            System.out.print("    ");
            for (int i = 0; i < array.length; i++) {
                printHorLane(array, i);
            }
        } else if (direction.equals("west")) {
            for (int i = array.length-1; i >= 0; i--) {
                printHorLane(array, i);
            }
        }
    }

    // builds visual for our vertical lanes
    private void printVerLane(Vehicle[] array, int i) {
        if (array[i] instanceof Car) {
            System.out.println("          | C |");
        } else if (array[i] instanceof Truck) {
            System.out.println("          | T |");
        } else if (array[i] instanceof Semi) {
            System.out.println("          | S |");
        } else if (array[i] instanceof Emergency) {
            System.out.println("          | E |");
        } else {
            System.out.println("          |   |");
        }
    }

    // builds visual for our horizontal lanes
    private void printHorLane(Vehicle[] array, int i) {
        if (array[i] instanceof Car) {
            System.out.print(" C");
        } else if (array[i] instanceof Truck) {
            System.out.print(" T");
        } else if (array[i] instanceof Semi) {
            System.out.print(" S");
        } else if (array[i] instanceof Emergency) {
            System.out.print(" E");
        } else {
            System.out.print("  ");
        }
    }

    // adds a vehicle to the specified lane, check for emergency
    private void addVehicle(Vehicle[] lane, boolean isEmergency) {

        for (int i = 0; i < lane.length; i++) {
            if (lane[i] == null) {

                if (isEmergency) {
                    lane[i] = new Emergency();
                    return;
                } else {
                    int vehicleType = (int) (Math.random() * ((3 - 1) + 1)) + 1;
                    randomVehicle(lane, i, vehicleType);
                    return;
                }
            }
        }
    }

    // adds a random vehicle to the specified lane at location i, of type vehicleType
    private void randomVehicle(Vehicle[] lane, int i, int vehicleType) {
        switch (vehicleType) {
            case 1:
                lane[i] = new Car();
                break;
            case 2:
                lane[i] = new Truck();
                break;
            case 3:
                lane[i] = new Semi();
                break;
        }
    }

    // boolean method, returns true if the intersection is empty
    private boolean isEmpty() {//Vehicle[] north, Vehicle[] south, Vehicle[] east, Vehicle[] west

        ArrayList<Vehicle> intersection = new ArrayList<>();
        Collections.addAll(intersection, north);
        Collections.addAll(intersection, south);
        Collections.addAll(intersection, east);
        Collections.addAll(intersection, west);

        for (Vehicle vehicle : intersection) {
            if (vehicle != null) {
                return false;
            }
        }

        return true;
    }

    // boolean method, return true if there are any emergency vehicles in the intersection
    private boolean hasEmergency() {

        ArrayList<Vehicle> intersection = new ArrayList<>();
        Collections.addAll(intersection, north);
        Collections.addAll(intersection, south);
        Collections.addAll(intersection, east);
        Collections.addAll(intersection, west);

        for (Vehicle vehicle : intersection) {
            if (vehicle instanceof Emergency) {
                return true;
            }
        }

        return false;
    }

    // boolean method, returns true if there is an emergency vehicle in the specified lane
    private boolean laneHasEmergency(Vehicle[] lane) {

        for (Vehicle vehicle : lane) {
            if (vehicle instanceof Emergency) {
                return true;
            }
        }

        return false;
    }

    // method to shift emergency vehicle to front of array
    private void shiftEmergency(Vehicle[] array) {

        // create a location variable to be used later
        int location = 0;

        if (laneHasEmergency(array)) {

            // find the location of our emergency vehicle
            for (int i = 0; i < array.length; i++) {
                if (array[i] instanceof Emergency) {
                    location = i;
                    break;
                }
            }

            // store our emergency vehicle
            Vehicle emergency = array[location];

            // shift all other vehicles backwards
            for (int i = location; i > 0; i--) {
                array[i] = array[i-1];
            }

            // put our stored emergency vehicle back in at the front
            array[0] = emergency;
        }
    }

    // method to shift our emergency vehicle, wherever it is, and change the light
    private void moveEmergency() {
        if (laneHasEmergency(north)) {
            shiftEmergency(north);
            northSouthGreen = true;
        } else if (laneHasEmergency(south)) {
            shiftEmergency(south);
            northSouthGreen = true;
        } else if (laneHasEmergency(east)) {
            shiftEmergency(east);
            eastWestGreen = true;
        } else if (laneHasEmergency(west)) {
            shiftEmergency(west);
            eastWestGreen = true;
        }
    }

    // method to 'shift' all cars up when one goes away
    private void shiftLane(Vehicle[] lane) {

        // remove the vehicle in front
        if (lane[0] != null) {
            lane[0] = null;
        }

        // shift all other vehicles forwards
        for (int i = 0; i < lane.length - 1; i++) {
            lane[i] = lane[i + 1];
        }

        // delete that pesky final vehicle
        lane[lane.length - 1] = null;
    }

    public boolean getRunning() {
        return this.running;
    }


    // method to run one iteration of the simulation
    public void runMovement() {

            if (northSouthGreen) {

                moveCars(north, south);

                northSouthGreen = false;
                eastWestGreen = true;

                placeVehicleRanLane();

                if (hasEmergency()) {
                    moveEmergency();
                }

            } else if (eastWestGreen) {

                moveCars(east, west);

                eastWestGreen = false;
                northSouthGreen = true;

                placeVehicleRanLane();

                if (hasEmergency()) {
                    moveEmergency();
                }
            }
    }

    // method to place a random vehicle type in a random lane
    private void placeVehicleRanLane() {
        if (isEmpty()) {
            running = false;
        } else {

            int randomLane = (int) (Math.random() * ((4 - 1) + 1)) + 1;
            Random isEmergency = new Random();
            boolean emergency = isEmergency.nextBoolean();
            switch (randomLane) {

                case 1:
                    addVehicle(north, emergency);
                    break;
                case 2:
                    addVehicle(south, emergency);
                    break;
                case 3:
                    addVehicle(east, emergency);
                    break;
                case 4:
                    addVehicle(west, emergency);
                    break;
            }
        }
    }

    // method to check if there are cars left in a given direction,
    // remove the cars if there is enough time left in the light
    private void moveCars(Vehicle[] arr, Vehicle[] arr2) {

        int timeLeft = LIGHT_DURATION;

        while (timeLeft > 0) {

            if (arr[0] == null && arr2[0] == null) {
                timeLeft = 0;

            } else if ((arr[0] != null && arr[0].getTime() < timeLeft) && (arr2[0] != null && arr2[0].getTime() < timeLeft)) {

                if (arr[0].getTime() >= arr2[0].getTime()) {
                    timeLeft -= arr[0].getTime();
                } else {
                    timeLeft -= arr2[0].getTime();
                }

                shiftLane(arr);
                shiftLane(arr2);

            } else if (arr[0] != null && arr[0].getTime() < timeLeft) {
                timeLeft -= arr[0].getTime();
                shiftLane(arr);
            } else if (arr2[0] != null && arr2[0].getTime() < timeLeft) {
                timeLeft -= arr2[0].getTime();
                shiftLane(arr2);
            } else {
                timeLeft = 0;
            }
        }
    }
}
