package vehicles;

public abstract class Truck extends Vehicle {

    // default constructor
    public Truck() {
        super(10, false);
    }

    // returns this car's light pass time
    @Override
    public int getTime() {
        return 10;
    }

    // returns whether this car is an emergency vehicle
    @Override
    public boolean getIsEmergency() {
        return false;
    }

    public void print() {
        System.out.println("");
    }
}
