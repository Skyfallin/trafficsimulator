package vehicles;

public class Emergency extends Vehicle {

    // default constructor
    public Emergency() {
        super(3, true);
    }

    // returns our time
    @Override
    public int getTime() {
        return 3;
    }

    // this is true, yes
    @Override
    public boolean getIsEmergency() {
        return true;
    }
}
