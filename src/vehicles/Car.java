package vehicles;

public class Car extends Vehicle {

    // default constructor
    public Car() {
        super(5, false);
    }

    // returns the time this car takes to pass
    @Override
    public int getTime() {
        return 5;
    }

    // returns whether this vehicle is an emergency
    @Override
    public boolean getIsEmergency() {
        return false;
    }
}
