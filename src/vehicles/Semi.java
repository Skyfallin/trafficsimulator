package vehicles;

public class Semi extends Vehicle {

    // default constructor
    public Semi() {
        super(15, false);
    }

    // returns our time
    @Override
    public int getTime() {
        return 15;
    }

    // not an emergency vehicle
    @Override
    public boolean getIsEmergency() {
        return false;
    }
}
