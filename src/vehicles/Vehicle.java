package vehicles;

public abstract class Vehicle {

    // globals
    private int time;
    private boolean isEmergency;

    // default constructor
    public Vehicle() {
        time = 0;
        isEmergency = false;
    }

    // constructor
    public Vehicle(int time, boolean isEmergency) {
        this.time = time;
        this.isEmergency = isEmergency;
    }

    // abstract methods
    abstract public int getTime();
    abstract public boolean getIsEmergency();

    void logicTest() {
        System.out.println("test");
    }

}
