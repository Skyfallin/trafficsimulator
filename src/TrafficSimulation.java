import street.TrafficLight;

import java.util.Scanner;

public class TrafficSimulation {

    public static void main(String[] args) {

        // grab our variables
        TrafficLight trafficLight = new TrafficLight();
        boolean running = trafficLight.getRunning();
        Scanner scan = new Scanner(System.in);

        // display the state of the intersection
        trafficLight.displayIntersection();

        // run one iteration of the simulation, wait for user to continue
        while (running) {

            trafficLight.runMovement();
            running = trafficLight.getRunning();

            trafficLight.displayIntersection();

            System.out.println("Press 'enter' to continue.");
            scan.nextLine();
        }

        System.out.println("The intersection is now clear. Have a good day!");
        scan.close();
    }
}
